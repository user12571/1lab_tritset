#include <iostream>
#include <gtest/gtest.h>
#include "tritset.h"
int main() {
    //testing::initGoogleTest(&argc,argv);
    return RUN_ALL_TESTS();
}
TEST(operators,assign) //присваивание, проверка длины
{
    tritset A(16);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[17] = True;
    A[18] = Unknown;
    A[19] = False;
    A[49] = Unknown;
    EXPECT_EQ(A[0],True);
    EXPECT_EQ(A[1],Unknown);
    EXPECT_EQ(A[2],False);
    EXPECT_EQ(A[3],Unknown);
    EXPECT_EQ(A[8],Unknown);
    EXPECT_EQ(A[17],True);
    EXPECT_EQ(A[18],Unknown);
    EXPECT_EQ(A[19],False);
    EXPECT_EQ(A[45],Unknown);
    EXPECT_EQ(A[3000000],Unknown);
    EXPECT_EQ(A.GetLength(),20);
}
TEST(trim,trim)
{
    tritset A(16);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[17] = True;
    A[18] = Unknown;
    A[19] = False;
    A[40] = False;
    A[45] = True;
    A[49] = Unknown;
    A.trim(44);
    EXPECT_EQ(A.GetLength(),41); //проверка, ищется ли последний
    EXPECT_EQ(A[40],False);

    A[40] = False;
    A[41] = True;
    A.trim(41);
    EXPECT_EQ(A.GetLength(),42); //проверка, ровно ли до того места удаляется
    EXPECT_EQ(A[41],True);
    EXPECT_EQ(A[42],Unknown);

    A[40] = False;
    A[41] = True;
    A.trim(40);
    EXPECT_EQ(A.GetLength(),41); //проверка, ищется ли
    EXPECT_EQ(A[41],Unknown);
}

TEST(operators,AND)
{
    tritset A(48);
    tritset B(25);
    A[0] = True;
    B[0] = False;

    A[1] = Unknown;
    B[1] = False;

    A[2] = False;
    B[2] = False;

    A[3] = True;
    B[3] = Unknown;

    A[4] = Unknown;
    B[4] = Unknown;

    A[5] = False;
    B[5] = Unknown;

    A[6] = True;
    B[6] = True;

    A[7] = Unknown;
    B[7] = True;

    A[8] = False;
    B[8] = True;

    A[78] = False;
    A[94] = Unknown;
    B[39]= True;

    tritset C(1);
    C = A & B;
    EXPECT_EQ(C[0],False);
    EXPECT_EQ(C[1],False);
    EXPECT_EQ(C[2],False);

    EXPECT_EQ(C[3],Unknown);
    EXPECT_EQ(C[4],Unknown);
    EXPECT_EQ(C[5],False);

    EXPECT_EQ(C[6],True);
    EXPECT_EQ(C[7],Unknown);
    EXPECT_EQ(C[8],False);

    EXPECT_EQ(C[39],Unknown);
    EXPECT_EQ(C[9999],Unknown);
    EXPECT_EQ(C[78],False);
    EXPECT_EQ(C[94],Unknown);

    EXPECT_EQ(C.GetLength(),79);
}

TEST(operators,OR)
{
    tritset A(48);
    tritset B(25);
    A[0] = True;
    B[0] = False;

    A[1] = Unknown;
    B[1] = False;

    A[2] = False;
    B[2] = False;

    A[3] = True;
    B[3] = Unknown;

    A[4] = Unknown;
    B[4] = Unknown;

    A[5] = False;
    B[5] = Unknown;

    A[6] = True;
    B[6] = True;

    A[7] = Unknown;
    B[7] = True;

    A[8] = False;
    B[8] = True;

    A[78] = False;
    A[94] = Unknown;
    B[39]= True;

    tritset C(1);
    C = A | B;
    EXPECT_EQ(C[0],True);
    EXPECT_EQ(C[1],Unknown);
    EXPECT_EQ(C[2],False);

    EXPECT_EQ(C[3],True);
    EXPECT_EQ(C[4],Unknown);
    EXPECT_EQ(C[5],Unknown);

    EXPECT_EQ(C[6],True);
    EXPECT_EQ(C[7],True);
    EXPECT_EQ(C[8],True);

    EXPECT_EQ(C[39],True);
    EXPECT_EQ(C[9999],Unknown);
    EXPECT_EQ(C[78],Unknown);
    EXPECT_EQ(C[94],Unknown);

    EXPECT_EQ(C.GetLength(),40);
}


TEST(operators,NOT)
{
    tritset A(48);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[42] = True;
    A[100] = Unknown;
    A[128] = False;
    A[9999] = Unknown;
    A = ~A;
    EXPECT_EQ(A[0],False);
    EXPECT_EQ(A[1],Unknown);
    EXPECT_EQ(A[2],True);


    EXPECT_EQ(A[42],False);
    EXPECT_EQ(A[100],Unknown);
    EXPECT_EQ(A[128],True);
    EXPECT_EQ(A[9999],Unknown);
    EXPECT_EQ(A.GetLength(),129);
}

TEST(cardionality_test,return_trit)
{
    tritset A(48);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[3] = Unknown;
    A[4] = Unknown;
    A[5] = False;
    A[6] = True;
    A[7] = Unknown;
    A[8] = False;
    A[78] = False;
    A[80] = False;
    A[94] = Unknown;
    EXPECT_EQ(A.cardinality(True),2);
    EXPECT_EQ(A.cardinality(Unknown),74);
    EXPECT_EQ(A.cardinality(False),5);
}

TEST(cardionality_test,return_map)
{
    tritset A(48);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[3] = Unknown;
    A[4] = Unknown;
    A[5] = False;
    A[6] = True;
    A[7] = Unknown;
    A[8] = False;
    A[78] = False;
    A[80] = False;
    A[94] = Unknown;
    EXPECT_EQ(A.cardinality()[True],2);
    EXPECT_EQ(A.cardinality()[Unknown],74);
    EXPECT_EQ(A.cardinality()[False],5);
}
TEST(construct,len)
{
    tritset A(48);
    EXPECT_EQ(A.GetLength(),0);
}
TEST(construct,copy)
{
    tritset A(48);
    A[0] = True;
    A[1] = Unknown;
    A[2] = False;
    A[3] = Unknown;
    A[4] = Unknown;
    A[5] = False;
    A[6] = True;
    A[7] = Unknown;
    A[8] = False;
    A[78] = False;
    A[80] = False;
    A[94] = Unknown;
    tritset B=A;
    EXPECT_EQ(B.GetLength(),81);
    EXPECT_EQ(B[0],True);
    EXPECT_EQ(B[1],Unknown);
    EXPECT_EQ(B[2],False);
    EXPECT_EQ(B[3],Unknown);
    EXPECT_EQ(B[4],Unknown);
    EXPECT_EQ(B[5],False);
    EXPECT_EQ(B[6],True);
    EXPECT_EQ(B[7],Unknown);
    EXPECT_EQ(B[8],False);
    EXPECT_EQ(B[30],Unknown);
    EXPECT_EQ(B[78],False);
    EXPECT_EQ(B[80],False);
    EXPECT_EQ(B[94],Unknown);
    EXPECT_EQ(B[95],Unknown);

}
