//
// Created by computer on 19.10.2018.
//

#ifndef UNTITLED1_TRITSET_H
#define UNTITLED1_TRITSET_H

#include <vector>
#include <unordered_map>
// 1lab.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
typedef unsigned int size_t;
typedef unsigned int arr_t;
enum trit { False=-1, Unknown, True };
#define ARR_LEN (lenght - 1) / (sizeof(arr_t) * 4) + 1
trit operator&(const trit _Left, const trit _Right);
trit operator~(const trit T);
trit operator|(const trit _Left, const trit _Right);
class tritset
{
private:
    std::vector <arr_t> ntrit; //массив, в котором хранятся триты
    size_t lenght; //физическая длина в тритах
    size_t LOGIC_lenght; //логическая длина в тритах
    void SetLenght(size_t newlen); //можно public

    tritset& set(size_t _Pos, trit _Val);

    trit _Subscript(size_t _Pos) const;
public:
    size_t GetLength(); //+
    typedef std::unordered_map< trit, size_t, std::hash<int> > Mymap;
    Mymap cardinality(); //+
    size_t cardinality(trit sample); //+

    void trim(size_t lastIndex);//+
    tritset(size_t l);
    tritset(const tritset &obj); //
    void print_bits(size_t Pos_Uint); //TEST
    void print_trits(size_t Pos_Uint); //TEST

    void operator=(const tritset& _Right);

    bool operator==(const tritset& _Right); //TEST


    /*constexpr*/ trit operator[](size_t _Pos) const;

    /*inline*/ tritset operator&(tritset _Right);
    /*inline*/ tritset operator|(tritset _Right);
    /*inline*/ tritset operator~();

    class reference
    {
        friend tritset;
    private:
        tritset *_Pbitset;	// битсет
        size_t _Mypos;	// позиция в битсете
    public://
        ~reference();
        reference(tritset& _Bitset, size_t _Pos);
        reference& operator=(trit _Val);
        reference& operator=(const reference& _Bitref);

        operator trit() const;
    };
    reference operator[](size_t _Pos);
};

#endif //UNTITLED1_TRITSET_H
