//
// Created by computer on 19.10.2018.
//

#include "tritset.h"

typedef unsigned int size_t;
typedef unsigned int arr_t;
//enum trit { False=-1, Unknown, True };
#define ARR_LEN (lenght - 1) / (sizeof(arr_t) * 4) + 1
trit operator&(const trit _Left, const trit _Right)
{
    return((_Left < _Right) ? _Left : _Right);//return(min(_Left, _Right));
}
trit operator~(const trit T)
{
    return((trit)-T);//return((trit)(-(T-2)));
}
trit operator|(const trit _Left, const trit _Right)
{
    return((_Left < _Right) ? _Right : _Left);//return(max(_Left, _Right));
}





void tritset::SetLenght(size_t newlen) //можно public
    {
        ntrit.resize( (newlen - 1) / (sizeof(arr_t) * 4)  + 1); //для нуля можно отдельный if чтобы не выделялась память, но зачем, не хочется каждый раз при изменении длины в него заходить //ARR_LEN
        for (size_t i = (lenght - 1) / (sizeof(arr_t) * 4) + 1; i < (newlen - 1) / (sizeof(arr_t) * 4) + 1; i++) //ARR_LEN
        {
            ntrit[i] = 0;
        }
        lenght = newlen;
    }

tritset& tritset::set(size_t _Pos, trit _Val) //В битсете возвращается значение, поэтому у меня тоже
    {	// set bit at _Pos to _Val
        if (lenght <= _Pos)
        {
            if (_Val == Unknown)
            {
                return (*this);
            }
            //ntrit.insert(ntrit.begin() + (lenght - 1) / (sizeof(size_t) * 4) + 1, (_Pos - lenght + 1) / (sizeof(size_t) * 4), 0); //ERROR
            SetLenght(_Pos + 1);
        }
        if ((LOGIC_lenght < _Pos + 1) && _Val != Unknown)
        {
            LOGIC_lenght = _Pos + 1;
        }
        if ((LOGIC_lenght == _Pos + 1) && _Val == Unknown)
        {
            size_t i = _Pos - 1;
            for (; i > 0 && _Subscript(i) == Unknown; i--);
            LOGIC_lenght = i + 1;
        }
        ntrit[2 * _Pos / (8 * sizeof(arr_t))] = (~(3 << _Pos * 2)  &  (ntrit[2 * _Pos / (8 * sizeof(size_t))])) + (((_Val + 3) % 3) << _Pos * 2); //ARR_LEN
        return (*this);
    }

trit tritset::_Subscript(size_t _Pos) const
    {
        if (lenght <= _Pos)
        {
            return Unknown;
        }
        arr_t t = ((ntrit[2 * _Pos / (8 * sizeof(arr_t))]) >> ((2 * _Pos) % (sizeof(arr_t) * 8))) & 3; //ARR_LEN
        switch (t) //можно без свитча, если 0 1 2 делать, а не -1 0 1
        {
            case 2:
                return (False);
            case 0:
                return (Unknown);
            case 1:
                return (True);
            default:
                break;
        }
        //trit t_on_pos = (trit)(((ntrit[2 * _Pos / (8 * sizeof(size_t))]) >> ((2 * _Pos) % (sizeof(size_t) * 8))) & 3); //в начале заполняются нулями несколько бит а потом туда плюсом закидывается нужное число
        //return (t_on_pos);
        return (Unknown); //чтобы если что программа не падала
    }
size_t tritset::GetLength()
    {
        return(LOGIC_lenght);
    }
typedef std::unordered_map< trit, size_t, std::hash<int> > Mymap;
Mymap tritset::cardinality()
    {
        Mymap MAP;
        //MAP.insert(Mymap::value_type(False, cardinality(False)));
        MAP[False] = cardinality(False);
        MAP[Unknown] = cardinality(Unknown);
        MAP[True] = cardinality(True);
        return(MAP);
    }
size_t tritset::cardinality(trit sample)
    {
        size_t count = 0;
        for (size_t _Tpos = 0; _Tpos < LOGIC_lenght; _Tpos += 1)
        {
            if (sample == (*this)[_Tpos])
            {
                count++;
            }
        }
        return count;
    }

void tritset::trim(size_t lastIndex)
    {
        if ((LOGIC_lenght <= lastIndex + 1))
        {
            return;
        }
        size_t i = lastIndex;
        for (; i > 0 && (_Subscript(i) == Unknown); i--); //ищем последний не Unknown ИЛИ доходим до 0
        SetLenght(i + 1);
        LOGIC_lenght = i + 1;
        return;

    }

tritset::tritset(size_t l)
    {
        LOGIC_lenght = 0;
        SetLenght(l);

        //for (size_t i = 0; i <= l; i++) //неверно . тут не нужно занулять
        //{
        //	ntrit[i] = 0;
        //}
    }
tritset::tritset(const tritset &obj)
    {
        LOGIC_lenght = obj.LOGIC_lenght;
        SetLenght(obj.lenght);

        for (size_t Pos = 0; Pos < (lenght - 1) / (sizeof(arr_t) * 4) + 1; Pos += 1) //ARR_LEN
        {
            ntrit[Pos] = obj.ntrit[Pos];
        }
    }
void tritset::print_bits(size_t Pos_Uint) //TEST
    {
        size_t N = ntrit[Pos_Uint];
        arr_t mask = 1;
        for (size_t i = 0; i < sizeof(arr_t) * 8; i++)
        {
            printf("%d ",N&mask);
            N = N >> 1;
        }
        printf("\n");
    }
void tritset::print_trits(size_t Pos_Uint) //TEST
    {
        char dictionary[4] = { 'U', 'T', 'F', 'E' };
        size_t N = ntrit[Pos_Uint];
        arr_t mask = 3;
        for (size_t i = 0; i < sizeof(arr_t) * 8/2; i++)
        {
            printf("%c ", dictionary[N&mask]);
            N = N >> 2;
        }
        printf("\n");
    }

void tritset::operator=(const tritset& _Right)
    {
        SetLenght(_Right.lenght);
        LOGIC_lenght = _Right.LOGIC_lenght;
        for (long long i = 0; i < (lenght - 1) / (sizeof(size_t) * 4) + 1; i++) // len*2 перевод в биты, потом /8*sizeof перевод в байты //ARR_LEN
            ntrit[i] = _Right.ntrit[i];
    }

bool tritset::operator==(const tritset& _Right) //TEST
    {
        if (LOGIC_lenght != _Right.LOGIC_lenght)
        {
            return(false);
        }
        for (long long i = 0; i < (LOGIC_lenght - 1) / (sizeof(size_t) * 4) + 1; i++) // len*2 перевод в биты, потом /8*sizeof перевод в байты //ARR_LEN
        {
            if(ntrit[i] != _Right.ntrit[i])
                return(false);
        }
        return(true);
    }


/*constexpr*/ trit tritset::operator[](size_t _Pos) const
    {
        return (_Subscript(_Pos));
    }

/*inline*/ tritset tritset::operator&(tritset _Right)
    {
        tritset _Result = *this;
        _Result.SetLenght(((*this).lenght < _Right.lenght) ? _Right.lenght : (*this).lenght);//_Result.lenght = ((*this).lenght < _Right.lenght) ? _Right.lenght : (*this).lenght;
        for (size_t _Tpos = 0; _Tpos < lenght; _Tpos += 1)
        {
            _Result[_Tpos] = _Result[_Tpos] & _Right[_Tpos];
        }
        return (_Result);
    }
/*inline*/ tritset tritset::operator|(tritset _Right)
    {
        tritset _Result = *this;
        for (size_t _Tpos = 0; _Tpos < lenght; _Tpos += 1)
        {
            _Result[_Tpos] = _Result[_Tpos] | _Right[_Tpos];
        }
        return (_Result);
    }
/*inline*/ tritset tritset::operator~()
    {
        tritset _Result = *this;
        for (size_t _Tpos = 0; _Tpos < lenght; _Tpos += 1)
        {
            _Result[_Tpos] = ~_Result[_Tpos];
        }
        return (_Result);
    }



tritset::reference::~reference()
        {	// destroy the object
        }
tritset::reference::reference(tritset& _Bitset, size_t _Pos)
                : _Pbitset(&_Bitset), _Mypos(_Pos)
        {	// construct from bitset reference and position
        }
tritset::reference& tritset::reference::operator=(trit _Val)
        {	// assign Boolean to element
            _Pbitset->set(_Mypos, _Val);
            return (*this);
        }//
tritset::reference& tritset::reference::operator=(const reference& _Bitref)
        {	// assign reference to element
            _Pbitset->set(_Mypos, trit(_Bitref));
            return (*this);
        }
tritset::reference::operator trit() const
        {	// return element
            return (_Pbitset->_Subscript(_Mypos));
        }
tritset::reference tritset::operator[](size_t _Pos)
    {
        return(reference(*this, _Pos));
    }
